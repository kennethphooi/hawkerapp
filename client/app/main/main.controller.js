(function() {
  
  angular
    .module('hawkerapp')
    .controller('MainCtrl', MainCtrl);

  MainCtrl.$inject = ['$state', 'PopulateContentSvc', '$scope', 'NgMap'];

  

  function MainCtrl($state, PopulateContentSvc, $scope, NgMap) {

    var vm = this;
  
    // Initializes the map background layout
    NgMap.getMap().then(function (map) {
      $scope.map = map;
    });

    // Populates content (previous user posts/reviews) that initializes when the screen loads
    PopulateContentSvc.grabLoc()
    .then(function(contents) {
    
    // Customize map background layout
    $scope.mapOptions = {
      styles: [
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "color": "#cccccc"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape.natural",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#dddddd"
            }
        ]
    },
    {
        "featureType": "landscape.man_made",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "weight": 1.01
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "weight": 1
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "saturation": 0
            },
            {
                "lightness": 0
            },
            {
                "gamma": 5
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#666666"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#aaaaaa"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#aaaaaa"
            }
        ]
    }
]	
    }
    // Customizes the marker on the map
    $scope.markerOptions = {
      icon: "http://cdn.jsdelivr.net/emojione/assets/png/1f366.png?v=2.2.5"
    }
    $scope.selectedReview = ''
    $scope.reviews = contents.data
    $scope.reviews.forEach((review, index) => {
      $scope.reviews[index]['pos'] = [review.lat, review.long]
    })
    })

     $scope.showInfo = function (event,desc) {
      console.log(desc)
    };
    // Previews the modal window that shows the previous users posts/reviews
    $scope.test = function (event,review) {
      $scope.selectedReview = review
       $('#modelWindow').modal('show');
    };
  
  }
})();
