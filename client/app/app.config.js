// Handles the UI-routing
(function () {
    angular
        .module("hawkerapp")
        .config(hawkerappConfig);
    hawkerappConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function hawkerappConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
        // For main page
            .state("Main", {
                url: "/main",
                views: {
                    "menu": {
                        templateUrl: "/app/menu/menu.html",
                        controller: "MenuCtrl as ctrl",
                    },
                    "content": {
                        templateUrl: "/app/main/main.html",
                        controller: 'MainCtrl as ctrl'
                    }
                },

            })
            // For login page
            .state("Login", {
                url: "/login",
                views: {
                    "menu": {
                        templateUrl: "/app/menu/menu.html",
                        controller: "MenuCtrl as ctrl",
                    },
                    "content": {
                        templateUrl: "/app/login/login.html",
                        controller: 'LoginCtrl as ctrl'
                    }
                },

            })
            // For register page
            .state("Register", {
                url: "/register",
                views: {
                    "menu": {
                        templateUrl: "/app/menu/menu.html",
                        controller: "MenuCtrl as ctrl",
                    },
                    "content": {
                        templateUrl: "/app/register/register.html",
                        controller: 'RegisterCtrl as ctrl'
                    }
                },
            })
            // For posting page
            .state("Post", {
                url: "/post",
                views: {
                    "menu": {
                        templateUrl: "/app/menu/menu.html",
                        controller: "MenuCtrl as ctrl",
                    },
                    "content": {
                        templateUrl: "/app/post/post.html",
                        controller: 'PostCtrl as uploadCtrl'
                    }
                },

            })
        // Declares state /main as the default home page
        $urlRouterProvider.otherwise("/main");


    }
})();
