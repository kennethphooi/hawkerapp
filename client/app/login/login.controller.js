(function () {
  angular
    .module('hawkerapp')
    .controller('LoginCtrl', LoginCtrl);

  LoginCtrl.$inject = ['PassportSvc', '$state'];

  function LoginCtrl(PassportSvc, $state) {
    var vm = this;
    vm.inputType = 'password';
    vm.user = {
      username: '',
      password: ''
    }
    vm.msg = '';
    vm.passwordCheckBox = false;
    vm.hideShowPassword = function () {
      if (vm.inputType == 'password')
        vm.inputType = 'text';
      else
        vm.inputType = 'password';
    };
    vm.login = login;

    function login() {
        console.log(vm.user)
      PassportSvc.login(vm.user)
        .then(function (result) {
          console.log("User: ", result);
          $state.go('Main');
          return true;
        })
        .catch(function (err) {
            console.log(err)
          console.log("incorrect username")
          vm.msg = 'Invalid Username or Password!';
          vm.user.username = vm.user.password = '';
          return false;
        });
    }
  }
})();