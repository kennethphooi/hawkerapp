(function () {
    angular
        .module("hawkerapp")
        .controller("PostCtrl", PostCtrl);

    PostCtrl.$inject = ["Upload", 'PopulateContentSvc'];

    function PostCtrl(Upload, PopulateContentSvc) {
        // Keep the bindable members of the controllers at the top
        var vm = this;
        vm.imgFile = null;
        vm.imgFileS3 = null;
        vm.comment = "";
        vm.commentS3 = "";
        vm.status = {
            message: "",
            code: 0
        };
        vm.statusS3 = {
            message: "",
            code: 0
        };
        vm.initialize = initialize();
       
        // Goolemaps Geocoding API
          var geocoder;
          var map;
          function initialize() {
                geocoder = new google.maps.Geocoder();
                var latlng = new google.maps.LatLng(1.3521, 103.8198);
                var mapOptions = {
                zoom: 11,
                center: latlng
            }
            map = new google.maps.Map(document.getElementById('map'), mapOptions);
            }

            function codeAddress() {
                var address = document.getElementById('address').value;
                geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == 'OK') {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
                });
            }

        // Exposed functions
        vm.getGeoTag = getGeoTag;
        vm.upload = upload;
        vm.uploadS3 = uploadS3;
        vm.uploadinformation = {
            title: "",
            desc: "",
            imgUrl: "",
            lat: "",
            long: ""
        };
        
        // Populates the lat & long input box once user types in the address
        function getGeoTag(){
        var address = document.getElementById('address').value;
            PopulateContentSvc.getGeoTag(address).
            then(function(geoTag) {
                vm.uploadinformation.lat = geoTag.data.results[0].geometry.location.lat;
                vm.uploadinformation.long = geoTag.data.results[0].geometry.location.lng
            })
        }

        // Upload to S3
        function upload() {
            // Upload configuration and invokation 
            Upload.upload({
                url: '/upload',
                data: {
                    "img-file": vm.imgFile,
                    "comment": vm.comment
                }
            }).then(function (result) {
                vm.status.message = "The image is saved successfully in: " + result.data.path;
                vm.status.code = 202;
            }).catch(function (err) {
                console.log(err);
                vm.status.message = "Fail to save the image.";
                vm.status.code = 400
            });

        };

        function uploadS3() {
            // Upload configuration and invokation 
            Upload.upload({
                url: '/uploadS3',           //Need to point to another route. Can't have both sending to the same route
                data: {
                    "img-file": vm.imgFileS3,
                    "comment": vm.commentS3
                }
            }).then(function (result) {
                vm.statusS3.message = "The image is saved successfully in: " + result.data.path;
                vm.statusS3.code = 202;
                vm.imageFromS3 = result.data.path;
                vm.uploadinformation.imgUrl = result.data.path;
                PopulateContentSvc.post(vm.uploadinformation)
                .then(function(result) {
                    return true
                })
             

            }).catch(function (err) {
                console.log(err);
                vm.statusS3.message = "Fail to save the image.";
                vm.statusS3.code = 400
            });

        };

       
    };
    
      
    }
)();