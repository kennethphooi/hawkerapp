(function () {
  angular
    .module('hawkerapp')
    .service('PopulateContentSvc', PopulateContentSvc);
  // To make the API calls
  PopulateContentSvc.$inject = ["$http"];

  function PopulateContentSvc($http) {
    var svc = this;
    svc.post = post;
    svc.grabLoc = grabLoc;
    svc.getGeoTag = getGeoTag;
    // To grab previous reviews made by users to populate upon page initialization
    function grabLoc() {
      return $http.get("/grabcontent");
    }
    // To upload new posts and pictures into the database when users post
    function post(uploadinformation) {
      return $http.post(
        "/postcontent", 
        uploadinformation)
    }
    // External API call to google API to obtain Lat & Long of the address that the user inputs
    function getGeoTag(addString) {
      return $http.get(
        "https://maps.googleapis.com/maps/api/geocode/json?address=" + addString + "&key=AIzaSyBz9A7vxVMaTwjGbgXpyF3T-LcgEoREd0Q"
      )
    }
  }

})();


