(function () {
  angular
    .module('hawkerapp')
    .service('PassportSvc', PassportSvc);
  // To make the API calls
  PassportSvc.$inject = ["$http"];

  function PassportSvc($http) {
    var svc = this;

    svc.login = login;
    svc.register = register;
  

    // When user logs in, checks to see if the user exists
    function login(user) {
      return $http.post(
        '/login',
        user
      );
    }
    // When user registers, creates new entry
    function register(user){
        return $http.post (
            '/register',
            user
        )
    }






  }

})();
