(function () {
    angular
        .module("hawkerapp")
        .controller("RegisterCtrl", RegisterCtrl);

    RegisterCtrl.$inject = ["$sanitize", "$state", 'PassportSvc'];

    function RegisterCtrl($sanitize, $state, PassportSvc) {
        var vm = this;
        vm.user = {
            emailAddress : "",
            username : "",
            firstName : "",
            lastName : "",
            password : "",
            confirmpassword : "",
        }
        // For new user registration. Calls on PassportSvc which carries out the API call to the server side
        vm.register = function () {
            console.log(vm.user)
            PassportSvc.register(vm.user)
                .then(function (result) {
                    console.log(result);
                    $state.go('Login');
                    return true;
                })
                .catch(function (err) {
                    return false;
                })
        };

    }
})();