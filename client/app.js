(function () {
    angular
        .module("hawkerapp", [
            "ui.router",
            "ngSanitize",
            "ngProgress",
            "ngFileUpload",
            "ngMap"
        ]); 
})();