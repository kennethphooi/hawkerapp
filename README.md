Project Title: Hawkerthon

Hawkerthon is a food sharing platform where users and non-users are able to post pictures of their hawker food and leave reviews.

Installing

Run these commands in terminal:

1.  npm i
2.  bower i


Running the program

To run the program having carried out the installation, run this command in the terminal:

1.  node server/app.js


Live example

For more information on Hawkerthon, you can visit "http://ec2-52-77-223-152.ap-southeast-1.compute.amazonaws.com:3001/" to view the app in live deployment.
