var MongoClient = require("mongodb").MongoClient;

var mongoDb;

var URL = 'mongodb://localhost:27017/mydatabase'

MongoClient.connect(URL, function(err, db) {
  if (err) return
  mongoDb = db;
})

let getDb = () => {
    return mongoDb;
}

module.exports = {
    getDb: getDb
}