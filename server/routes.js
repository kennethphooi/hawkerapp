let reviews = require('./models/reviews')
let users = require('./models/users')
var bcrypt   = require('bcryptjs');
var express = require('express');
var api = express.Router();

//AWS configurations
var config = require('./config');
var AWS = require('aws-sdk');
var multerS3 = require('multer-S3');
var multer = require("multer");

//Configure AWS and multer-s3
AWS.config.accessKeyId = config.AWS_ACCESS_KEY_ID;
AWS.config.secretAccessKey = config.AWS_SECRET_ACCESS_KEY;
AWS.config.region = config.AWS_S3_REGION;
var s3Bucket = new AWS.S3();

var uploadS3 = multer ({
    storage: multerS3({
        s3: s3Bucket,
        bucket: config.AWS_S3_BUCKET,
        metadata: function (req, file, cb) {
            cb(null, {fieldName: file.fieldname});
        },
        key: function(req, file, cb) {
            cb(null, Date.now()
                +'-'
                +file.originalname);
        }
    })
});
// To upload into S3 bucket
api.post("/uploadS3",
    uploadS3.single("img-file"),
    function(req, res) {
        console.log("Upload to S3...");
        console.log(req.file);
            res.status(202).json ({
                path: req.file.location
            });
        }
    );
// API route to add reviews or posts (Used in initial phase for Postman)
api.post('/addreview', function (req, res) {
    reviews.addReview(req.query.title, req.query.desc, req.query.lat, req.query.long).then(data => {
        res.json(data)
    })
        .catch(err => {
            res.json(err)
        })
});
// API route to find reviews or posts
api.get('/findreview', function (req, res) {
    reviews.findReview(req.query.id).then(data => {
        res.json(data)
    })
        .catch(err => {
            res.json(err)
        })
});
// API route to change reviews or posts
api.post('/changereview', function (req, res) {
    reviews.changeReview(req.query.id, req.query.title, req.query.desc, req.query.lat, req.query.long).then(data => {
        res.json(data)
    })
        .catch(err => {
            res.json(err)
        })
});
// API route to delete reviews or posts
api.delete('/deletereview', function (req, res) {
    reviews.deleteReview(req.query.id).then(data => {
        res.json(data)
    })
        .catch(err => {
            res.json(err)
        })
});
// API route to populate the main page upon initialization
api.get('/grabcontent', function (req, res) {
    reviews.populateReview().then(data => {
        res.json(data);

    })
        .catch(err => {
            res.json(err)
        })
})
// API route to add reviews or posts
api.post('/postcontent', function (req, res) {
    reviews.postContent(req.body.title, req.body.desc, req.body.imgUrl, req.body.lat, req.body.long).then(data => {
        res.json(data);
    })
        .catch(err => {
            res.json(err)
        })
});

//User management

// API call to add new user upon user registration
api.post('/adduser', function (req, res) {
    users.addUser(req.query.email, req.query.username, req.query.firstname, req.query.lastname, req.query.password).then(data => {
        res.json(data)
    })
        .catch(err => {
            res.json(err)
        })
});
// API call to find user
api.get('/finduser', function (req, res) {
    users.findUser(req.query.username).then(data => {
        res.json(data)
    })
        .catch(err => {
            res.json(err)
        })
});
// API call to change user details
api.post('/changeuser', function (req, res) {
    users.changeUser(req.query.email, req.query.username, req.query.firstname, req.query.lastname, req.query.password).then(data => {
        res.json(data)
    })
        .catch(err => {
            res.json(err)
        })
});
// API call to delete users
api.delete('/deleteusers', function (req, res) {
    users.deleteUser(req.query.username).then(data => {
        res.json(data)
    })
        .catch(err => {
            res.json(err)
        })
});
// API call for user login
api.post('/login', function (req, res) {
    console.log("login")
    console.log(req.body)
    users.findUser(req.body.username).then(data => {
        console.log(data)
        if (bcrypt.compareSync(req.body.password, data.password)) {
            console.log("User password match");
            res.json(data)

        } else {
            console.log("User password does not match");
            res.json(false)
        }
    })
        .catch(err => {
            console.log(err)
            res.json(err)
        })
});
// API call to register new user
api.post('/register', function (req, res) {
    console.log(req.body);
   users.addUser(req.body.emailAddress, req.body.username, req.body.firstName, req.body.lastName, req.body.password).then(data => {
        res.json(data)
    })
        .catch(err => {
            res.json(err)
        })
});

// Exporting
module.exports = api;