var express = require("express");

var app = express();

var path = require("path");
var mongo = require("./db/mongo");
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require("passport"); 
var config = require("./config");

const CLIENT_FOLDER = path.join(__dirname, '/../client');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(express.static(CLIENT_FOLDER));

// Initialize session
app.use(session({
    secret: "hawkerapp-secret",
    resave: false,
    saveUninitialized: true
}));

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());



app.use('/', require('./routes'))

app.listen(config.port, function () {
    console.log("Server running at http://localhost:" + config.port);
});
