let mongo = require('../db/mongo')
let ObjectId = require('mongodb').ObjectId

// To add reviews (Used only for Postman testing at initial stage)
let addReview = ((title, desc, lat, long) => {
    return new Promise ((resolve, reject) => {
        var collection = mongo.getDb().collection('reviews')
        collection.insert({title: title, desc: desc, lat: lat, long: long}, function(err, result) {
            if (err)
                reject(err)
            resolve(result)
        })
    })
})
// To search for reviews
let findReview = ((id) => {
    return new Promise ((resolve, reject) => {
        mongo.getDb().collection("reviews", function (err, collection){
            var query = {
                _id: new ObjectId(id)
            };
            collection.find(query).toArray()
            .then(function(results){
                resolve(results);
            })
            .catch(function(err){
                reject(err)
            })

        })
    })
})
// To change or alter reviews
let changeReview = ((id, title, desc, lat, long) => {
    return new Promise ((resolve, reject) => {
        mongo.getDb().collection("reviews", function (err, collection) {
            var filter = {
                _id: new ObjectId(id)
            };
            var update = {$set: {
                title: title,
                desc: desc,
                lat: lat,
                long: long
            }};
            var options = {upsert: true};
            collection.updateOne(filter, update, options)
            .then(function(results){
                resolve(results);
            })
            .catch(function(err){
                reject(err)
            })
        })
    })
})
// To delete reviews
let deleteReview = ((id) => {
    return new Promise ((resolve, reject) => {
        mongo.getDb().collection("reviews", function (err, collection){
            var filter = {
                _id: new ObjectId(id)
            };
            collection.deleteOne(filter)
            .then(function(results){
                resolve(results);
            })
            .catch(function(err){
                reject(err)
            })
        })
    })
})
// To populate reviews based on their ID when page initializes
let populateReview = ((id) => {
    return new Promise ((resolve, reject) => {
        mongo.getDb().collection("reviews", function (err, collection){
            collection.find().toArray()
            .then(function(results){
                resolve(results);
              
            })
            .catch(function(err){
                reject(err)
            })

        })
    })
})
// To upload and post new contents
let postContent = ((title, desc, imgUrl, lat, long) => {
    return new Promise ((resolve, reject) => {
        var collection = mongo.getDb().collection('reviews')
        collection.insert({title: title, desc: desc, imgUrl: imgUrl, lat: lat, long: long}, function(err, result) {
            if (err)
            reject(err)
            resolve(result)
            console.log(result)
        })
    })
})
// Exporting
module.exports = {
    addReview: addReview,
    findReview: findReview,
    changeReview: changeReview,
    deleteReview: deleteReview,
    populateReview: populateReview,
    postContent: postContent
}