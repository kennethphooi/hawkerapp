let mongo = require('../db/mongo')
var bcrypt   = require('bcryptjs');
let ObjectId = require('mongodb').ObjectId
// Adds users upon new user registration
let addUser = ((email, username, firstname, lastname, password) => {
    return new Promise ((resolve, reject) => {
        var collection = mongo.getDb().collection('users');
        var hash = bcrypt.hashSync(password)
        collection.insert({email: email, username: username, firstname: firstname, lastname: lastname, password: hash}, function(err, result) {
            if (err)
                reject(err)
            resolve(result)
        })
    })
})
// To find a certain user
let findUser = ((username) => {
    console.log("finduser: " + username)
    return new Promise ((resolve, reject) => {
        mongo.getDb().collection("users", function (err, collection){
            var query = {
                username: username
            };
            collection.findOne(query)
            .then(function(results){
                resolve(results);
            })
            .catch(function(err){
                reject(err)
            })

        })
    })
})
// To change user details
let changeUser = ((email, username, firstname, lastname, password) => {
    return new Promise ((resolve, reject) => {
        mongo.getDb().collection("users", function (err, collection) {
            var filter = {
                username: username
            };
            var update = {$set: {
                email: email,
                username: username,
                firstname: firstname,
                lastname: lastname,
                password: password
            }};
            var options = {upsert: true};
            collection.updateOne(filter, update, options)
            .then(function(results){
                resolve(results);
            })
            .catch(function(err){
                reject(err)
            })
        })
    })
})
// To delete a user
let deleteUser = ((username) => {
    return new Promise ((resolve, reject) => {
        mongo.getDb().collection("users", function (err, collection){
            var filter = {
                username: username
            };
            collection.deleteOne(filter)
            .then(function(results){
                resolve(results);
            })
            .catch(function(err){
                reject(err)
            })
        })
    })
})

// Exporting
module.exports = {
    addUser: addUser,
    findUser: findUser,
    changeUser: changeUser,
    deleteUser: deleteUser,
}